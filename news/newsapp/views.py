from django.shortcuts import render
from django.http import *
from newsapp.models import *
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate, login, logout
from urllib.parse import parse_qs
from django.contrib.auth.decorators import login_required
from django.http import Http404
from django.shortcuts import render, redirect, get_object_or_404
import json
authenticated = False
g_user = ""

@csrf_exempt
def login_validate(request):
    global authenticated
    global g_user
    username = request.GET.get('username')
    password = request.GET.get('password')
    #authenticate the user
    user = authenticate(username=username, password=password)
    if user is not None and user.is_active:
        #login the user
        login(request, user)
        g_user = request.user
        payload = "Successfully logged in"
        http_response = HttpResponse(payload)
        http_response['Content-Type'] = 'text/plain'
        http_response_code = 200
        http_response.reason_phrase = 'OK'
        authenticated = (request.user.is_authenticated)
        return http_response

    else:
        payload = "Wrong log in information"
        http_response = HttpResponse(payload, status=403)
        http_response['Content-Type'] = 'text/plain'
        http_response_code = 403
        http_response.reason_phrase = ""
        return http_response

@csrf_exempt
def logout_validate(request):
    global authenticated
    if authenticated:
        logout(request)
        payload = "Successfully logged out"
        http_response = HttpResponse(payload)
        http_response['Content-Type'] = 'text/plain'
        http_response_code = 200
        http_response.reason_phrase = "OK"
        authenticated=False
        g_user=None
        return http_response
    else:
        payload = "Not logged in"
        http_response = HttpResponse(payload, status=403)
        http_response['Content-Type'] = 'text/plain'
        http_response_code = 403
        http_response.reason_phrase = ""
        return http_response


@csrf_exempt
def post_a_story(request):
    global authenticated
    global g_user
    if authenticated:
        #if the user is logged in
        r_json = json.loads(request.body)
        headline = r_json['headline']
        category = r_json['category']
        region = r_json['region']
        details = r_json['details']

        #if the author exists
        try :
            author = Author.objects.get(user=g_user)
            ns = News_Stories.objects.create(story_headline=headline, story_category=category, story_region=region, story_details=details, story_author=author)
        #if the author does not exists
        except:
            new_author = Author(user=g_user, name=g_user.username)
            new_author.save()
            ns = News_Stories.objects.create(story_headline=headline, story_category=category, story_region=region, story_details=details, story_author=new_author)


        return HttpResponse(status=201)
    else:
        payload = "You need to log in to post a new story"
        http_response = HttpResponse(payload, status=503)
        http_response['Content-Type'] = 'text/plain'
        http_response_code = 503
        http_response.reason_phrase = "Service Unavailable"
        return http_response

@csrf_exempt
def  get_stories(request):
    category = request.GET['category']
    region = request.GET['region']
    date = request.GET['date']
    if category == '*':
        category = ''
    if region == '*':
        region = ''
    if date == '*':
        date = ''
    #filter it
    ns_search = News_Stories.objects.filter(story_category__contains=category, story_region__contains=region, story_date__contains=date).values()
    try:
        #this is the one to send back to the get request
        ns_search_list =[]
        for i in range (0,ns_search.count()):
            #get the author id and search the database
            authod_id = ns_search[i]['story_author_id']
            author = Author.objects.get(id=authod_id)
            #print(author.name)
            single_record = {'u_key' : ns_search[i]['id'],
                            'headline' : ns_search[i]['story_headline'],
                            'category' : ns_search[i]['story_category'],
                            'region' : ns_search[i]['story_region'],
                            'author' : author.name,
                            'date' : ns_search[i]['story_date'].isoformat(),
                            'details' : ns_search[i]['story_details']
                            }
            ns_search_list.append(single_record)

        payload = {"stories" : ns_search_list}
        http_response = HttpResponse(json.dumps(payload))
        http_response['Content-Type'] = 'application/json'
        return http_response

    except:
        payload = "No news found"
        http_response = HttpResponse(payload, status=404)
        http_response['Content-Type'] = 'text/plain'
        http_response_code = 404
        http_response.reason_phrase = ""
        return http_response

@csrf_exempt
def  delete_story(request):
    global authenticated
    global g_user
    if authenticated:
        #id_d = request.POST['id']
        r_json = json.loads(request.body)
        id_d = r_json['id']
        try:
            record_to_delete = News_Stories.objects.get(id=id_d)
            record_to_delete.delete()

            payload = "Successfully deleted the record"
            http_response = HttpResponse(payload, status=201)
            http_response['Content-Type'] = 'text/plain'
            http_response_code = 201
            http_response.reason_phrase = "CREATED"
            return http_response
        except:
            payload = "No such ID"
            http_response = HttpResponse(payload, status=503)
            http_response['Content-Type'] = 'text/plain'
            http_response_code = 503
            http_response.reason_phrase = "Service Unavailable"
            return http_response
    else:
        payload = "You need to log in"
        http_response = HttpResponse(payload, status=503)
        http_response['Content-Type'] = 'text/plain'
        http_response_code = 503
        http_response.reason_phrase = "Service Unavailable"
        return http_response
