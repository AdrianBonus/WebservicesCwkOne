from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Author(models.Model):
    user = models.ForeignKey(User , on_delete = models.CASCADE)
    name = models.CharField(max_length=64)

    def __str__(self):
        return self.user.username


class News_Stories(models.Model):
    story_headline = models.CharField(max_length=64)
    story_categories = [
                        ('pol', 'politics'),
                        ('art', 'technology news'),
                        ('tech', 'technology news'),
                        ('trivia', 'trivia news')
                        ]
    story_category = models.CharField(max_length=64, choices = story_categories, default = 'unknown')
    regions = [
                ('uk', 'United Kingdom News'),
                ('eu', 'European News'),
                ('w', 'World News')
                ]
    story_region = models.CharField(max_length=64, choices = regions, default = 'unknown')
    story_author = models.ForeignKey(Author , on_delete=models.CASCADE)
    story_date = models.DateTimeField(auto_now_add=True)
    story_details = models.CharField(max_length=512)
