import requests
import time
import sys
import json

def login_user(username, password, the_url):
    # username = "ammar"
    # password = "cwkpassword"
    #url = "http://sc16a2b.pythonanywhere.com//api/login/?username="+username+"&password="+password
    url = the_url + "//api/login/?username="+username+"&password="+password
    data = requests.post(url)
    print (str(data.status_code) + " " + data.reason)
    print (data.text)


#used to log out the user
def logout_user():
    payload = None
    url = "http://sc16a2b.pythonanywhere.com/api/logout/"
    data = requests.post(url)
    print (str(data.status_code) + " " + data.reason)
    print (data.text)

#used to post a story to the web service
def post_story(headline, category, region, details):
    payload={
        'headline' : headline,
        'category' : category,
        'region' : region,
        'details' : details
    }
    url = "http://sc16a2b.pythonanywhere.com/api/poststory/"
    data = requests.post(url, json=payload)
    print (str(data.status_code) + " " + data.reason)
    print (data.text)

#id is the id of the agency
#cat is the category of the News
#reg is the region
#date is the date the news was created
def get_story(id, cat, reg, date):
    print("")
    print("")
    #get url from agency code
    payload = {
        'category' : cat,
        'region' : reg,
        'date' : date
    }
    #change the first part of url to id
    try :
        s_url = get_url(id)
        url = s_url + "/api/getstories/"
        data = requests.get(url, params=payload)
        print (str(data.status_code) + " " + data.reason)
        print (data.json())
    except :
        url = "http://sc16a2b.pythonanywhere.com/api/getstories/"
        data = requests.get(url, params=payload)
        print (str(data.status_code) + " " + data.reason)
        print (data.json())


#delete a story
def delete_story(number):
    payload={
        'id' : number
    }
    url = "http://sc16a2b.pythonanywhere.com/api/deletestory/"
    data = requests.post(url, json=payload)
    print (str(data.status_code) + " " + data.reason)
    print (data.text)

#used to register myself
def register():
    payload={
        'agency_name' : 'Adrian Bonus News Agency',
        'url' : 'http://sc16a2b.pythonanywhere.com/',
        'agency_code' : 'ASB'
    }


    url = "http://directory.pythonanywhere.com/api/register/"
    #headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
    data = requests.post(url, json=payload)
    print (str(data.status_code) + " " + data.reason)
    print (data.text)

#used to get the url from agency code
def get_url(id):
    data = requests.get("http://directory.pythonanywhere.com/api/list/")
    jdata = data.json()

    for i in range (0,len(jdata["agency_list"])):
        if jdata["agency_list"][i]['agency_code'] == id:
            url = jdata["agency_list"][i]['url']
            return url
            break
    return None
#list all of the agencies
def list():
    data = requests.get("http://directory.pythonanywhere.com/api/list/")
    jdata = data.json()
    print(jdata)



def client():
    #keep running
    while 1==1:
        #check the argument 1.
        #ask user for inputs
        #pass the input to the log in function
        user_command = input("Please enter a command : ")
        user_command_split = user_command.split()
        for i in range (0, len(user_command_split)):
            user_command_split[i] = user_command_split[i].replace(' ','')
        if user_command_split[0] == "login":
            username = input("Please enter your username : ")
            password = input("Please enter your password : ")
            the_url = user_command_split[1]
            login_user(username, password, the_url)


        if user_command_split[0] == "logout":
            logout_user()


        if user_command_split[0] == "post":
            #get input from user and store in variable
            headline = input("Please enter the headline : ")
            print("")
            print("Available categories : pol, art, tech, trivia")
            category = input("Please enter the category : ")
            print("")
            print("Available regions are : uk, eu, w")
            region = input("Please enter the region : ")
            print("")
            details = input("Please enter the details of the news : ")
            post_story(headline, category, region,details)

        if user_command_split[0] == "news":
            #initialise the variables in which we would store values
            id = ""
            cat = ""
            reg = ""
            date = ""
            #for each of the index, check if it's one of the following
            for i in range (0,len(user_command_split)):
                try :
                    #if the index starts with id, then we know id exists in the command
                    if user_command_split[i].startswith("[-id="):
                        #get the string between the 2 speech marks
                        id = user_command_split[i][6:-2]
                    #if there is no string, then it must be *
                    if id == "":
                        id="*"
                except :
                    id = "*"


                try :
                    if user_command_split[i].startswith("[-cat="):
                        cat = user_command_split[i][7:-2]
                    if cat == "":
                        cat = "*"
                except :
                    cat = "*"


                try:
                    if user_command_split[i].startswith("[-reg="):
                        reg = user_command_split[i][7:-2]
                    if reg == "":
                        reg = "*"
                except :
                    reg = "*"


                try:
                    if user_command_split[i].startswith("[-date="):
                        date = user_command_split[i][8:-2]
                    if date == "":
                        date = "*"
                except :
                    date = "*"
            #get the story
            get_story(id, cat, reg, date)

        if user_command_split[0] == "list":
            list()

        if user_command_split[0] == "delete":
            number = user_command_split[1].split("_")[1]
            delete_story(int(number))

        print("")


client()
